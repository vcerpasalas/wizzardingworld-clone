# Wizzarding World Clone ⚡👓🏰🧙‍♂ - Reto 6 & 7

El presente proyecto constó de clonar la web oficlal "Wizzarding World" del universo de Harry Potter y Animales Fantásticos. Cabe resaltar que este proyecto de desarrollo web es hecho por fans para fans. En nuestra propuesta, se añadieron nuevas funcionalidades que permitirán que tu experiencia en el mundo mágico sea memorable.

Para comenzar, encontrarás un Header interactivo con secciones en las que puedes navegar y te direccionarán a nuevas páginas, como "News & Features". Podrás enterarte de las últimas novedades o leer artículos de interés. Así también, se cuenta con una sección llamada "Discover", donde verás cards con información y fotografías sobre libros, películas, experiencias, entre otros. Nuestra sección Hero propone una experiencia diferente, ingresa a la biblioteca de personajes donde podrás averiguar a que casa de Hogwarts perteneció cada personaje del universo HP y Animales Fantásitcos. Además, te presentamos un generador mágico de hechizos.

Al seguir navegando en la landing page, tendrás la posibilidad de escuchar la melodía de Hogwarts al presionar un botón y enterarte de las últimas noticias a través de secciones que cuentan con cards o un slider informativo y visual.

¿Vas a perderte esta nueva experiencia mágica?

[![logo.png](https://i.postimg.cc/LXdQ94j6/logo.png)](https://postimg.cc/mcwyVsd0)

## Introducción

El presente proyecto contiene los archivos .hmtl .css, .js y json, que fueron escritos mediante el editor Visual Studio Code. Por otra parte, contiene imágenes y audio (mp3). Por otra parte, el repositorio contiene archivos escritos en SASS y Bootstrap.
Te invitamos a que lo puedas descargar en tu máquina local, testear y revisar. Puedes guiarte de estas notas para revisar la página.

## Edición, creación y manejo de archivos json

Las APIS trabajadas en el presente proyecto fueron consideradas tomando como base la información presentada en la web Wizzarding World.

1) La web consume una API de personajes del universo de Harry Potter y Animales Fantásticos. Para fines exclusivos del proyecto, se trabajó a partir de la API de Personajes, obtenida de: https://hp-api.herokuapp.com/.
En la cual, se editó manualmente la información de cerca el 80% de personajes, añadiendo información respecto a la casa de Hogwarts a la que pertenece cada personaje y su fotografía. Adicionalmente se añadieron ciertos personajes del universo de Animales Fantásticos.
Finalmente se consumió la API alojada en el mismo proyecto, para la visualización de cada personaje en el buscador interactivo. Este código se encuentra en lenguaje JavaScript.

2) Se trabajó a partir de la siguiente API de libros https://fedeperin-harry-potter-api.herokuapp.com/libros, se editó la información añadiendo fotografías para cada libro. Así como todos los datos respecto a los libros del universo de Animales Fantásticos, entre otros.

3) Se trabajó a partir de la API de hechizos https://fedeperin-harry-potter-api.herokuapp.com/hechizos. Se añadieron 43 hechizos nuevos y se amplió la información de los hechizos exitentes.
   
5) Se crearon APIS desde cero, sobre juegos, obras de teatro y experiencias en relación al universo HP.
    
## Esquemas explicativos 
   
Manejo de APIS en la sección "Descubriendo" de la web.
    
[![esquema-apis.png](https://i.postimg.cc/tgJjq59d/esquema-apis.png)](https://postimg.cc/v4JjX5gD)

## Vistas del proyecto

### Vista pantalla completa en laptop
   
  
### Vista responsive en mobile


### Módulo

Este proyecto corresponde a la sexta y séptima semana del módulo de Frontend.

```
En total serán 10 proyectos, que corresponden a 10 retos.
```

### Temas vistos para el presente reto:

- Intersection Observer API
- Bootstrap
- SASS
- API Web
- Local Storage
- Session Storage
- Observer API
- Callbacks
- Promesas
- try... catch
- Métodos HTTP
- JSON
- Async Await
- Axios
- Fetch
- Layouts
- Componentes
- Modificadores
- JavaScript ES6
- Template string
- Objetos let y const
- Spread operator
- Destructuring objects
- Destructuring arrays
- Clases
- Módulos

Javascript 
HTML
CSS
UX/UI
Github Colaborativo

```
Todos los derechos reservados.
```

## Contribuciones

Te pedimos leas los códigos de conducta de GitHub y el presente "README.md" para poder utilizar y contribuir con el proyecto. Se recomienda el uso del proyecto únicamente para fines académicos y educativos. Prohibido su uso para fines comerciales.

## Autores

- **Cerpa Salas, Valeria**
- **Chacón Puris, Betsaida**
- **Chumbimune de la Cruz, Rosa**
- **Diez Silva, Gino**
- **Huamán Lazo, Diego**

## Licencia

Este proyecto contiene una licencia MIT - ver el archivo adjunto en el repositorio para conocer los detalles.

## Agradecimientos

- Profesor Elliot Garamendi, por la enseñanza
- Fundación Es Hoy y CODIGO, por la oportunidad de formarnos en desarrollo web.
